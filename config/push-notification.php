<?php

return array(

    'switch_dev_ios'     => array(
        'environment' =>'development',
        'certificate' => storage_path()."/apn/pushcert_develop.pem",
        'passPhrase'  =>'',
        'service'     =>'apns'
    ),
    'switch_pro_ios'     => array(
        'environment' =>'production',
        'certificate' => storage_path()."/apn/pushcert_prodcut.pem",
        'passPhrase'  =>'',
        'service'     =>'apns'
    ),
    'appNameAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'yourAPIKey',
        'service'     =>'gcm'
    )
);