<?php

namespace App;

use App\SwFileEntryModel;
use Illuminate\Database\Eloquent\Model;

class SwUserModel extends Model
{
	const USER_STATUS_RESERVED = 10;
	const USER_STATUS_APPROVED = 20;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $fillable = ['email', 'name', 'password', 'college', 'mobile', 'status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('avatar_id', 'token', 'updated_at', 'created_at', 'password', "deviceType", "deviceToken", "latitude", "longitude");

	// public function getAvatarAttribute() {
	// 	$this->attributes['avatar'] = 
	// }

	static public function createUser($email, $name, $password, $college, $mobile, $status = SwUserModel::USER_STATUS_RESERVED) {
		return parent::create([
			'email' => $email,
			'name' => $name,
			'password' => md5($password),
			'college' => $college,
			'mobile' => $mobile,
			'status' => $status
		]);
	}

	static public function user($email, $password) {
		$entry = SwUserModel::where('email', $email)
			->where('password', md5($password))
			//->where('status', SwUserModel::USER_STATUS_APPROVED)
			->first();
		return $entry;
	}

	static public function getToken($email, $password) {
		$entry = SwUserModel::user($email, $password);
		if ($entry != NULL) {
			$entry->token = md5(uniqid(rand(), true));
			$entry->save();
		}
		return $entry;
	}

	static public function verifyToken($token) {
		$entry = SwUserModel::where('token', $token)
			//->where('status', SwUserModel::USER_STATUS_APPROVED)
			->first();
		return $entry;
	}

	public function changePassword($oldPassword, $newPassword) {
		if ($this->password != md5($oldPassword))
			return false;
		$this->password = md5($newPassword);
		$this->save();
		return true;
	}
}
