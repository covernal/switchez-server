<?php

namespace App;

use App\SwFileEntryModel;
use Illuminate\Database\Eloquent\Model;

class SwMatchModel extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'matchs';

	protected $fillable = [];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('park_id', 'updated_at', 'created_at');
}
