<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\SwFileEntryModel;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class SwFileEntryController extends BaseController
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$entries = Fileentry::all();
 
		return view('fileentries.index', compact('entries'));
	}
 
 	static public function _add(Request $request, $fileEntry = 'file', $owner_id = 0, $subpath='avatar/') {
		$file = $request->file($fileEntry);
		$extension = $file->getClientOriginalExtension();
		$rand_timestamp = "_".time();

		$saved_path = $subpath.$file->getClientOriginalName().$rand_timestamp.'.'.$extension;
		Storage::disk('s3')->put($saved_path, File::get($file));

		$entry = new SwFileEntryModel();
		$entry->mime = $file->getClientMimeType();
		$entry->name = $file->getClientOriginalName();
		$entry->path = $saved_path;
		$entry->user_id = $owner_id;

		$entry->save();
 
		return $entry;	
	}

	public function add(Request $request, $fileEntry = 'file', $owner_id = 0, $subpath='') {
		$entry = $this->_add($request, $fileEntry, $owner_id, $subpath);
 
		return response()->json(array(
			'result' => 'success',
			'response' => $entry
		));		
	}

	static public function download(Request $request, $id) {
		$entry = SwFileEntryModel::find($id);
		if ($entry == NULL)
			return response()->json(array(	
				'result' => 'not found'			
			), 404);
		else {
			$file = Storage::disk('s3')->get($entry->path);

			return (new Response($file, 200))
				->header('Content-Type', $entry->mime);
		}
	}

	static public function _delete(Request $request, $id) {
		$entry = SwFileEntryModel::find($id);
		if ($entry == NULL)
			return false;
		else {
			$file = Storage::disk('s3')->delete($entry->path);
			$entry->delete();
			return true;
		}
	}

	public function delete(Request $request, $id) {
		$result = $this->_delete($request, $id);
		if ($result == false)
			return response()->json(array(	
				'result' => 'not found'			
			), 404);
		else {
			return response()->json(array(
				'result' => 'deleted'));
		}
	}
}
