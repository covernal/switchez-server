<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\SwUserModel;
use App\SwCollegeModel;

use Illuminate\Http\Response;
use Illuminate\Http\Request;


class SwCollegeController extends Controller
{
	public function add(Request $request) {		
		try
		{
			$entry = new SwCollegeModel();

			$entry->college = $request->name;
			$entry->save();

			return response()->json(array(
				'result' => 'success',
				'response' => $entry
			));
		}
		catch(\Exception $e)
		{
			return response()->json(array(
				'result' => 'fail',
				'error' => $e
			), 400);
		}
	}

	public function index(Request $request) {
		$entries = SwCollegeModel::all();
		
		return response()->json(
			$entries
		);
	}
}
