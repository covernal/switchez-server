<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\SwUserModel;
use App\SwLocationModel;
use App\SwMatchModel;
use App\SwDeviceModel;

use Illuminate\Http\Response;
use Illuminate\Http\Request;

use DB;

function sendPush($user_id, $message, $data) {
	$user = SwUserModel::where('id', $user_id)->first();
	if ($user == null) return;

	if ($user->deviceToken == null || $user->deviceToken == ' ') {
		return;
	}

	PushNotification::app("switch_dev_ios")
		->to($user->deviceToken)
		->send(PushNotification::Message($message, $data));
}

class SwMatchController  extends Controller
{
	const ARROUND_RANGE = 1; // mile
	// status 0 - wait for arriver
	// status 1 - go to leaver
	// status 2 - arround of leaver
	// status 3 - switching
	// status 10 - switch as success
	// status 20 - cancel to switch

	public function getDeparting(Request $request, $park_id)
	{
		$entries = SwMatchModel::where('park_id', $park_id)
						->where('status', 0)->get();

		return response()->json(array(
				'result' => 'success',
				'response' => $entries
		));
	}

	public function requireDeparting(Request $request, $park_id)
	{
		$userEntry = $request->auth_user;
		$user_id = $userEntry->id;

		SwMatchModel::where('leave_user_id', $user_id)
						->where('status', '<', 10)
						->delete();

		$entry = new SwMatchModel();
		$entry->leave_user_id = $user_id;
		$entry->park_id = $park_id;
		$entry->status = 0; // wait for arriver
		$entry->save();

		return response()->json(array(
				'result' => 'success',
				'response' => $entry
		));
	}

	public function requireArriving(Request $request, $match_id)
	{
		$userEntry = $request->auth_user;
		$user_id = $userEntry->id;

		$entry = SwMatchModel::where('id', $match_id)->first();
		if ($entry == null) {
			return response()->json(array(
				'result' => 'not found'
			), 400);
		}

		if ($entry->status != 0) {
			return response()->json(array(
				'result' => 'bad request'
			), 404);
		}

		$entry->arrive_user_id = $user_id;
		$entry->status = 1; // arriver is going to leaver
		$entry->save();

		sendPush($entry->leave_user_id, 
			$userEntry->name." picked you up to switch.", 
			[
				'type' => 'match',
				'status' => 1,
				'extra' => $userEntry				
			]);

		return response()->json(array(
				'result' => 'success',
				'response' => $entry
		));
	}

	public function requireSwitch(Request $request, $match_id)
	{
		$userEntry = $request->auth_user;
		$user_id = $userEntry->id;

		$entry = SwMatchModel::where('id', $match_id)->first();
		if ($entry == null) {
			return response()->json(array(
				'result' => 'not found'
			), 400);
		}

		if ($entry->arrive_user_id != $user_id) {
			return response()->json(array(
				'result' => 'bad request'
			), 404);
		}

		$entry->status = 2; // arriver is arround of leaver
		$entry->save();

		sendPush($entry->leave_user_id, 
			"Your switcher ".$userEntry->name." is arround of you.", 
			[
				'type' => 'match',
				'status' => $entry->status
			]);

		return response()->json(array(
				'result' => 'success',
				'response' => $entry
		));
	}

	public function beginSwitch(Request $request, $match_id)
	{
		$userEntry = $request->auth_user;
		$user_id = $userEntry->id;

		$entry = SwMatchModel::where('id', $match_id)->first();
		if ($entry == null) {
			return response()->json(array(
				'result' => 'not found'
			), 400);
		}

		if ($entry->arrive_user_id != $user_id && $entry->leave_user_id != $user_id) {
			return response()->json(array(
				'result' => 'bad request'
			), 404);
		}

		$opp_id = $entry->arrive_user_id == $user_id ? $entry->leave_user_id : $entry->arrive_user_id;

		$entry->status = 3; // now switching
		$entry->save();

		return response()->json(array(
				'result' => 'success',
				'response' => $entry
		));
	}

	public function completeSwitch(Request $request, $match_id)
	{
		$userEntry = $request->auth_user;
		$user_id = $userEntry->id;

		$entry = SwMatchModel::where('id', $match_id)->first();
		if ($entry == null) {
			return response()->json(array(
				'result' => 'not found'
			), 400);
		}

		if ($entry->arrive_user_id != $user_id && $entry->leave_user_id != $user_id) {
			return response()->json(array(
				'result' => 'bad request'
			), 404);
		}

		$opp_id = $entry->arrive_user_id == $user_id ? $entry->leave_user_id : $entry->arrive_user_id;

		$entry->status = 10; // completed
		$entry->save();

		sendPush($opp_id,
			"The switching of ".$userEntry->name." has completed.", 
			[
				'type' => 'match',
				'status' => $entry->status			
			]);

		return response()->json(array(
				'result' => 'success',
				'response' => $entry
		));
	}

	public function cancelSwitch(Request $request, $match_id)
	{
		$userEntry = $request->auth_user;
		$user_id = $userEntry->id;

		$entry = SwMatchModel::where('id', $match_id)->first();
		if ($entry == null) {
			return response()->json(array(
				'result' => 'not found'
			), 400);
		}

		if ($entry->arrive_user_id != $user_id && $entry->leave_user_id != $user_id) {
			return response()->json(array(
				'result' => 'bad request'
			), 404);
		}

		$entry->status = 20; // canceled
		$entry->save();

		$opp_id = $entry->arrive_user_id == $user_id ? $entry->leave_user_id : $entry->arrive_user_id;

		sendPush($opp_id,
			"Your switcher ".$userEntry->name." has canceled.", 
			[
				'type' => 'match',
				'status' => $entry->status			
			]);

		return response()->json(array(
				'result' => 'success',
				'response' => $entry
		));
	}
}