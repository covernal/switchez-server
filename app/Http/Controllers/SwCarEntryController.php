<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\SwUserModel;
use App\SwCarModel;
use App\Http\Controllers\SwFileEntryController;

use Illuminate\Http\Response;
use Illuminate\Http\Request;


class SwCarEntryController extends Controller
{
	public function index(Request $request) {
		$userEntry = $request->auth_user;
		$cars = SwCarModel::where("user_id", $userEntry->id)->get();
		return response()->json(array(
				'cars' => $cars
		));
	}

	public function add(Request $request) {		
		try
		{
			$userEntry = $request->auth_user;

			$entry = new SwCarModel();
			$entry->user_id = $userEntry->id;
			$entry->maker = $request->input('maker');
			$entry->model = $request->input('model');
			$entry->color = $request->input('color');

			$entry->save();

			return response()->json(array(
				'result' => 'success',
				'response' => $entry
			));
		}
		catch(Exception $e)
		{
			return response()->json(array(
				'result' => 'fail'
			), 400);
		}
	}

	public function update(Request $request, $id) {
		try
		{
			$userEntry = $request->auth_user;
			$carEntry = SwCarModel::find($id);

			$carEntry->maker = $request->input('maker');
			$carEntry->model = $request->input('model');
			$carEntry->color = $request->input('color');

			$carEntry->save();

			return response()->json(array(
				'result' => 'success',
				'response' => $carEntry
			));
		}
		catch(Exception $e)
		{
			return response()->json(array(
				'result' => 'fail'
			), 400);
		}		
	}

	public function getUserDefaultCar(Request $request, $id/*user_id*/) {
		$userEntry = SwUserModel::find($id);
		if ($userEntry == NULL) {
			return response()->json(array(
				'result' => 'bad request'
			), 400);
		}

		$carEntry = SwCarModel::where('user_id', $userEntry->id)
		->first();

		if ($carEntry == NULL) {
			return response()->json(array(
				'result' => "not found user's default car"
			), 404);
		}
		else {
			return response()->json($carEntry);
		}
	}

	public function getMineDefaultCar(Request $request) {
		$userEntry = $request->auth_user;
		return $this->getUserDefaultCar($request, $userEntry->id);
	}

	public function updateMineDefaultCar(Request $request) {
		try
		{
			$userEntry = $request->auth_user;
			$carEntry = SwCarModel::where('user_id', $userEntry->id)
			->first();

			if ($carEntry == NULL) {
				return $this->add($request);
			}
			else {
				return $this->update($request, $carEntry->id);
			}
		}
		catch(Exception $e)
		{
			return response()->json(array(
				'result' => 'fail'
			), 400);
		}		
	}

	public function getCarPhoto (Request $request, $id) {
		$carEntry = SwCarModel::find($id);
		if ($carEntry == NULL) {
			return response()->json(array(
				'result' => 'bad request'
			), 400);
		}
		else if ($carEntry->photo_id == NULL) {
			return response()->json(array(	
				'result' => 'not found car photo'
			), 404);	
		}

		return SwFileEntryController::download($request, $carEntry->photo_id);
	}

	public function downloadPhoto(Request $request, $id) {
		$userEntry = $request->auth_user;
		$carEntry = SwCarModel::find($id);
		if ($carEntry == NULL || $carEntry->user_id != $userEntry->id) {
			return response()->json(array(
				'result' => 'bad request'
			), 400);
		}
		else if ($carEntry->photo_id == NULL) {
			return response()->json(array(	
				'result' => 'not found'			
			), 404);	
		}
		
		return SwFileEntryController::download($request, $carEntry->photo_id);
	}

	public function uploadPhoto(Request $request, $id) {
		$userEntry = $request->auth_user;
		$carEntry = SwCarModel::find($id);

		if ($carEntry == NULL || $carEntry->user_id != $userEntry->id) {
			return response()->json(array(
				'result' => 'bad request'
			), 400);
		}

		SwFileEntryController::_delete($request, $carEntry->photo_id);

		$fileEntry = SwFileEntryController::_add($request, 'file', $userEntry->id, 'car/');

		$carEntry->photo_id = $fileEntry->id;
		$carEntry->save();

		return response()->json(array(
				'result' => 'uploaded'));
	}

	public function getUserDefaultCarPhoto(Request $request, $id/*user_id*/) {
		$userEntry = SwUserModel::find($id);
		if ($userEntry == NULL) {
			return response()->json(array(
				'result' => 'bad request'
			), 400);
		}

		$carEntry = SwCarModel::where('user_id', $userEntry->id)
			->first();

		if ($carEntry == NULL) {
			return response()->json(array(
				'result' => 'bad request'
			), 400);
		}
		else {
			return $this->getCarPhoto($request, $carEntry->id);
		}			
	}

	public function getMineDefaultCarPhoto(Request $request) {
		$userEntry = $request->auth_user;
		return $this->getUserDefaultCarPhoto($request, $userEntry->id);			
	}

	public function updateMineDefaultCarPhoto(Request $request) {
		$userEntry = $request->auth_user;
		$carEntry = SwCarModel::where('user_id', $userEntry->id)
		->first();

		if ($carEntry == NULL) {
			return response()->json(array(
				'result' => 'bad request'
			), 400);

		}
		else {
			return $this->uploadPhoto($request, $carEntry->id);
		}	
	}
}
