<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\SwUserModel;
use App\SwLocationModel;
use App\SwRateModel;
use App\Http\Controllers\SwFileEntryController;

use Illuminate\Http\Response;
use Illuminate\Http\Request;


class SwUserEntryController extends Controller
{
	public function add(Request $request) {		
		try
		{
			$entry = SwUserModel::createUser(
				$request->input('email'),
				$request->input('name'),
				$request->input('password'),
				$request->input('college'),
				$request->input('mobile'),
				//SwUserModel::USER_STATUS_RESERVED
				SwUserModel::USER_STATUS_APPROVED
			);

			$entry = SwUserModel::getToken($request->input('email'), $request->input('password'));

			$locEntry = new SwLocationModel();
			$locEntry->user_id = $entry->id;
			$locEntry->save();
			return response()->json(array(
				'result' => 'success',
				'response' => $entry
			));
		}
		catch(\Exception $e)
		{

			return response()->json(array(
				'result' => 'fail',
				//'error' => $e
			), 400);
		}
	}

	public function login(Request $request) {
		$email = $request->input('email');
		$password = $request->input('password');

		$entry = SwUserModel::getToken($email, $password);
		
		if ($entry == NULL) {
			return response()->json(array(
				'result' => 'unauthorized'
			), 401);
		}
		else if ($entry->status == SwUserModel::USER_STATUS_APPROVED) {
			return response()->json(array(
				'result' => 'success',
				'token' => $entry->token,
				'me' => $entry
			));
		}
		else {
			return response()->json(array(
				'result' => 'fail'
			), 400);
		}
	}

	public function addRate(Request $request, $touser_id, $rate)
	{
		try {
			$from = $request->auth_user;
			SwRateModel::addRate($from->id, $touser_id, $rate);			

			return $this->getInfo($touser_id);
		}
		catch(\Exception $e) {
			return response()->json(array(
					'result' => 'fail'
				), 400);
		}
	}

	public function getInfo($id) {
		$entry = SwUserModel::find($id);

		$ratePoint = SwRateModel::getRatePoint($id);

		$entry = $entry->toArray();
		$entry["rate"] = $ratePoint;

		return response()->json(array(
				'result' => 'success',
				'response' => $entry
		));
	}


	public function updateInfo(Request $request, $id) {
		$entry = SwUserModel::find($id);

		//$entry->email = $request->input('email');
		$entry->name = $request->input('name');
		//$entry->password = $request->input('password');
		$entry->college = $request->input('college');
		$entry->mobile = $request->input('mobile');

		$entry->save();

		return response()->json(array(
			'result' => 'success',
			'response' => $entry
		));
	}

	public function getMe(Request $request) {
		$userEntry = $request->auth_user;
		return $this->getInfo($userEntry->id);
	}

	public function updateMe(Request $request) {
		$userEntry = $request->auth_user;

		return $this->updateInfo($request, $userEntry->id);
	}

	public function setDeviceToken(Request $request) {
		$userEntry = $request->auth_user;

		$userEntry->deviceType = $request->type;
		$userEntry->deviceToken = $request->token;

		$userEntry->save();

		return response()->json(array(
			'result' => 'success',
			'response' => $userEntry
		));
	}

	public function changePassword(Request $request) {
		$oldPassword = $request->input('oldPassword');
		$newPassword = $request->input('newPassword');

		$entry = $request->auth_user; //SwUserModel::find($email, $oldPassword);
		if ($entry != NULL) {
			if ($entry->changePassword($oldPassword, $newPassword) == true)
				return response()->json(array(
					'result' => 'success',
					'response' => 'changed with new password'
				));
		}

		return response()->json(array(
			'result' => 'fail'
		), 400);
	}

	public function downloadAvatar(Request $request, $id) {
		$entry = SwUserModel::find($id);
		if ($entry == NULL) {
			return response()->json(array(
				'result' => 'bad request'
			), 400);
		}
		else if ($entry->avatar_id == NULL) {
			return response()->json(array(	
				'result' => 'not found'			
			), 404);	
		}
		
		return SwFileEntryController::download($request, $entry->avatar_id);
	}

	public function downloadMineAvatar(Request $request) {
		$userEntry = $request->auth_user;
		return $this->downloadAvatar($request, $userEntry->id);
	}

	public function uploadAvatar(Request $request) {
		$userEntry = $request->auth_user;
		SwFileEntryController::_delete($request, $userEntry->avatar_id);

		$fileEntry = SwFileEntryController::_add($request, 'file', $userEntry->id, 'avatar/');

		$userEntry->avatar_id = $fileEntry->id;
		$userEntry->save();

		return response()->json(array(
				'result' => 'uploaded'));
	}
}
