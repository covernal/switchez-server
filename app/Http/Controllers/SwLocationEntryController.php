<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\SwUserModel;
use App\SwLocationModel;
use App\Http\Controllers\SwFileEntryController;

use Illuminate\Http\Response;
use Illuminate\Http\Request;


class SwLocationEntryController  extends Controller
{
	public function index(Request $request, $id) {
		$userEntry = SwUserModel::find($id);
		if ($userEntry == null) {
			return response()->json(array(
				'result' => 'fail'
			), 400);
		}

		return response()->json([
				'latitude' => $userEntry->latitude,
				'longitude' => $userEntry->longitude
			]);
	}

	public function update(Request $request) {		
		$userEntry = $request->auth_user;

		$lat = $request->input('lat');
		$lng = $request->input('lng');

		$userEntry->latitude = $lat;
		$userEntry->longitude = $lng;
		$userEntry->save();

		return response()->json(array(
			'result' => 'success'
		));
	}
}
