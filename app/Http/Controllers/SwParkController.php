<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\SwUserModel;
use App\SwParkModel;

use Illuminate\Http\Response;
use Illuminate\Http\Request;


class SwParkController extends Controller
{
	public function add(Request $request, $college_id) {		
		try
		{
			$entry = new SwParkModel();

			$entry->college_id = $college_id;
			$entry->park = $request->name;
			$entry->save();

			return response()->json(array(
				'result' => 'success',
				'response' => $entry
			));
		}
		catch(\Exception $e)
		{
			return response()->json(array(
				'result' => 'fail'
			), 400);
		}
	}

	public function update(Request $request, $college_id, $park_id)
	{
		try
		{
			$entry = SwParkModel::where('id', $park_id)->first();

			if ($entry == null)
				return response()->json(array(
					'result' => 'fail',
				), 400);

			$entry->park = $request->input('name');
			$entry->save();

			return response()->json(array(
				'result' => 'success',
				'response' => $entry
			));
		}
		catch(\Exception $e)
		{
			return response()->json(array(
				'result' => 'fail'
			), 400);
		}
	}

	public function delete(Request $request, $college_id, $park_id)
	{
		try
		{
			$entry = SwParkModel::where('id', $park_id)->first();

			if ($entry == null)
				return response()->json(array(
					'result' => 'fail',
				), 400);

			$entry->delete();

			return response()->json(array(
				'result' => 'success'
			));
		}
		catch(\Exception $e)
		{
			return response()->json(array(
				'result' => 'fail'
			), 400);
		}
	}

	public function index(Request $request, $college_id) {
		$entries = SwParkModel::where('college_id', $college_id)->get();
		
		return response()->json(
			$entries
		);
	}
}
