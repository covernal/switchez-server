<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('signup', 'SwUserEntryController@add');
Route::get('login', 'SwUserEntryController@login');

Route::get('user/{id}/avatar', 'SwUserEntryController@downloadAvatar');
Route::get('user/{id}', 'SwUserEntryController@getInfo');

Route::get('user/{id}/car', 'SwCarEntryController@getUserDefaultCar');
Route::get('user/{id}/car/photo', 'SwCarEntryController@getUserDefaultCarPhoto');

Route::group(['middleware' => ['api']], function() {
	Route::put('me/password', 'SwUserEntryController@changePassword');
	Route::get('me/avatar', 'SwUserEntryController@downloadMineAvatar');
	Route::post('me/avatar', 'SwUserEntryController@uploadAvatar');

	Route::get('me', 'SwUserEntryController@getMe');
	Route::post('me', 'SwUserEntryController@updateMe');
	Route::post('me/deviceToken', 'SwUserEntryController@setDeviceToken');

	Route::get('me/car', 'SwCarEntryController@getMineDefaultCar');
	Route::post('me/car', 'SwCarEntryController@updateMineDefaultCar');
	Route::get('me/car/photo', 'SwCarEntryController@getMineDefaultCarPhoto');
	Route::post('me/car/photo', 'SwCarEntryController@updateMineDefaultCarPhoto');

	Route::get('me/cars', 'SwCarEntryController@index');

	Route::post('user/{id}/rate/{rate}', 'SwUserEntryController@addRate');	
	
	Route::get('user/{id}/location', 'SwLocationEntryController@index');
	Route::post('me/location', 'SwLocationEntryController@update');

	//Route::post('match/{lat}/{lng}/{range}/{prevDistance?}', 'SwMatchController@getMatch');

	Route::get('match/{park_id}/list', 'SwMatchController@getDeparting');

	Route::get('match/{park_id}/requireDeparting', 'SwMatchController@requireDeparting');
	Route::get('match/{martch_id}/requireArriving', 'SwMatchController@requireArriving');
	Route::get('match/{martch_id}/requireSwitch', 'SwMatchController@requireSwitch');
	Route::get('match/{martch_id}/beginSwitch', 'SwMatchController@beginSwitch');
	Route::get('match/{martch_id}/completeSwitch', 'SwMatchController@completeSwitch');
	Route::get('match/{martch_id}/cancelSwitch', 'SwMatchController@cancelSwitch');

});



Route::post('media','SwFileEntryController@add');

Route::get('colleges','SwCollegeController@index');
Route::post('admin/college','SwCollegeController@add');

Route::get('college/{id}/parks','SwParkController@index');
Route::post('admin/college/{id}/park','SwParkController@add');
Route::put('admin/college/{id}/park/{park_id}','SwParkController@update');
Route::delete('admin/college/{id}/park/{park_id}','SwParkController@delete');
