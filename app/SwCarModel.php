<?php

namespace App;

use App\SwFileEntryModel;
use Illuminate\Database\Eloquent\Model;

class SwCarModel extends Model
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cars';

	protected $fillable = ['maker', 'model', 'color', 'user_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('photo_id', 'updated_at', 'created_at');

	// public function getAvatarAttribute() {
	// 	$this->attributes['avatar'] = 
	// }
}
