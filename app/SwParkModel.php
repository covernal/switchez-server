<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SwParkModel extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'parks';

	protected $fillable = [];

	protected $hidden = ['created_at', 'updated_at'];
}
