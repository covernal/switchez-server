<?php

namespace App;

use App\SwFileEntryModel;
use Illuminate\Database\Eloquent\Model;

class SwLocationModel extends Model
{
	const PHONE_LOC = 1;
	const CAR_LOC = 2	;

	const STATUS_NOPARKING = 0;
	const STATUS_ARRIVING = -1;
	const STATUS_LEAVING = -2;
	const STATUS_MEETING = -3;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'locations';

	protected $fillable = [];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('id', 'updated_at', 'created_at');
}
