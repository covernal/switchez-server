<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SwDeviceModel extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'devices';

	protected $fillable = ['user_id', 'token'];

	protected $hidden = array('id', 'updated_at', 'created_at');

	public function regDeviceToken($user_id, $token, $type) {
		$entry = SwDeviceModel::where('user_id', $user_id)->first();

		if ($entry == NULL) {
			$entry = new SwDeviceModel();
			$entry->user_id = $user_id;
			$entry->token = $token;
			$entry->type = $type;
			$entry->save();

			return $entry;
		}
		else {
			$entry->token = $token;
			$entry->type = $type;
			$entry->save();

			return $entry;
		}
	}

	static public function getDeviceToken($user_id) {
		$entry = SwDeviceModel::where('user_id', $user_id)->first();

		if ($entry == NULL) return NULL;

		return $entry;
	}
}
