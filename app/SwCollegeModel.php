<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SwCollegeModel extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'colleges';

	protected $fillable = [];

	protected $hidden = ['created_at', 'updated_at'];
}
