<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SwRateModel extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'rates';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('id', 'updated_at', 'created_at');

	static public function addRate($from_user_id, $to_user_id, $rate) {
		$entry = new SwRateModel();
		$entry->from_user_id = $from_user_id;
		$entry->to_user_id = $to_user_id;
		$entry->rate = $rate;
		$entry->save();

		return $entry;
	}

	static public function getRatePoint($user_id) {
		$ratePoints = DB::table('rates')
			->select(DB::raw('avg(rate) as point'))
			->where('to_user_id', $user_id)
			->get();
		return count($ratePoints) == 0 ? 0 : 
				is_null($ratePoints[0]->point) ? 0 : $ratePoints[0]->point;
	}
}
