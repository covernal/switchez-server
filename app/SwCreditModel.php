<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SwCreditModel extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'credits';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('id', 'updated_at', 'created_at');

	static public function addCredit($user_id, $credit_count) {
		$entry = SwCreditModel::where('user_id', $user_id)->first();

		if ($entry == NULL) {
			$entry = new SwCreditModel();
			$entry->user_id = $user_id;
			$entry->credit = $credit_count;
			$entry->save();

			return $entry;
		}
		else {
			$entry->credit += $credit_count;
			$entry->save();

			return $entry;
		}
	}

	static public function getCreditPoint($user_id) {
		$entry = SwCreditModel::where('user_id', $user_id)->first();
		if ($entry == NULL) {
			return 0;
		}
		else {
			return $entry->credit;
		}
	}
}
